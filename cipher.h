#ifndef CIPHER_H
#define CIPHER_H
#include "string"
using namespace std;
class Cipher
{
public:
    Cipher();
    Cipher(string data, string pass);
    virtual string encrypt()=0;
    virtual string decrypt()=0;
protected:
    string input_data,password;
};

#endif // CIPHER_H
