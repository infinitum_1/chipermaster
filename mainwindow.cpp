#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QCoreApplication>
#include<QString>
#include <iostream>
#include<string>
using namespace std;
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect((*ui).processButton,SIGNAL(released()),this,SLOT(handleProcessBtn()));

}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::handleProcessBtn(){
    string input_data =ui->inputEditText->toPlainText().toStdString();
    cout<<"out data: "<<input_data<<endl;
}

