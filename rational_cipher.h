#ifndef RATIONAL_CIPHER_H
#define RATIONAL_CIPHER_H
#include <cipher.h>
class RationalCipher: public Cipher{

    public:
    RationalCipher(string input, string password);
public:
  string encrypt();

public:
  string decrypt();

};
#endif // RATIONAL_CIPHER_H
