#ifndef RATIONALCIPHER_H
#define RATIONALCIPHER_H

#include "cipher.h"



class RationalCipher : public Cipher
{
public:
    RationalCipher(string data, string pass);
    string encrypt() override;
    string decrypt() override;
private:
    struct Letter{
        char letter;
        string* mas_combs;
        int size;
        int current;
    };
    Letter* array_letters;
    void fill_array_letters();

};

#endif // RATIONALCIPHER_H
